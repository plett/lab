Build https://github.com/lighttiger2505/lab into a Docker container.

I run it with something like this in my .bashrc:

```bash
if [ -x $(/usr/bin/which docker) ]; then
    alias lab='docker run -v ~/.config/lab:/home/app/.config/lab registry.gitlab.com/plett/lab:latest lab'
fi
```

