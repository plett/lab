FROM alpine:latest as builder

ENV LAB_VERSION 0.6.2

RUN apk --no-cache add ca-certificates
WORKDIR /root/
ADD https://github.com/lighttiger2505/lab/releases/download/v${LAB_VERSION}/lab-v${LAB_VERSION}-linux-amd64.tar.gz lab.tar.gz
RUN tar xvzf lab.tar.gz

FROM alpine:latest
RUN apk --no-cache add ca-certificates
RUN addgroup -S app && adduser -S -G app app
USER app
COPY --from=builder /root/linux-amd64/lab /usr/local/bin/lab
CMD ["/usr/local/bin/lab", "--version"]
